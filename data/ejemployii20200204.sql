﻿DROP DATABASE IF EXISTS ejemployii20200204;
CREATE DATABASE ejemployii20200204;
USE ejemployii20200204;

CREATE TABLE alumnos (
    id_alumno int AUTO_INCREMENT PRIMARY key,
    nombre varchar(255),
    apellidos varchar(255),
    imagen varchar(255)
);

INSERT INTO alumnos(nombre,apellidos, imagen)
	VALUES 
    ( 'Carlos', 'Lopez', 'f (1)'),
    ( 'Antonio', 'Ibanez', 'f (2)'),
    ( 'Oscar', 'Megia', 'f (4)'),
    ( 'Ramon', 'Abramo', 'f (5)'),
    ( 'Pablo', 'Rodriguez', 'f (6)'),
    ( 'Ruben', 'Gomez', 'f (7)'),
    ( 'David', 'Aguirre', 'f (8)'),
    ( 'Andres', 'Perez', 'f (9)'),
    ( 'Lucia', 'Gomez', 'f (10)'),
    ( 'Carmen', 'Rodiguez', 'f (11)'),
    ( 'Vernica', 'Sanchez', 'f (12)'),
    ( 'Roberto', 'Sanchez', 'f (13)') 
    ;