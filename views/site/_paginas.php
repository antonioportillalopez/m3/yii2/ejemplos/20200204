<!-- este es una forma simple de listado tal cual viene de index.php
<div>
<h1><?= $model->id_alumno ?></h1>
<li><?= $model->nombre ?></li>
<li><?= $model->apellidos ?></li>
</div>
-->


<!--  Este es una forma de listado con bootstrap (en el cual automaticamente pagina)   -->

  <div class="col-sm-6 col-md-4">
    <div class="thumbnail">
        <img src="<?= Yii::getAlias("@web").'/fotos/'.$model->imagen.'.jpg' ?>" width="80" height="80"></img>
      <!--<img src="..." alt="...">-->
      <div class="caption">
          <h3 class="regla1">Id Alumno <?= $model->id_alumno ?></h3>
        <p>
            <li>Nombre: <?= $model->nombre ?></li>
            <li>Apellidos: <?= $model->apellidos ?></li>
        </p>
        <!--<p><a href="#" class="btn btn-primary" role="button">Button</a> <a href="#" class="btn btn-default" role="button">Button</a></p>-->
      </div>
    </div>
  </div>

